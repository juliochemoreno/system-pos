const { withSuperjson } = require('next-superjson')

module.exports = withSuperjson()({
  experimental: {
    serverComponentsExternalPackages: ["@prisma/client", "bcryptjs"]
  },
})