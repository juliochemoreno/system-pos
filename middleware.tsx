import { withAuth } from "next-auth/middleware"

export default withAuth({
  secret: process.env.SECRET,
  pages: {
    signIn: '/auth/login',
  },
  callbacks: {
    authorized({ req, token }) {
      if (req.nextUrl.pathname === "/admin") {
        return token?.role === "admin"
      }
      return !!token
    },
  }
})

export const config = { matcher: ["/", "/admin", "/dashboard", "/profile", "/companies/:path*", "/company/:path*", "/settings/:path*"]};