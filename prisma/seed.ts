import { hash } from "bcryptjs";
import { PrismaClient, Prisma, ProductType, UserRole, UserType } from '@prisma/client';
import { Value } from "sass";

const prisma = new PrismaClient();
const data = [
  {
    name: "Admin",
    email: "elsitiobacano1@gmail.com",
    password: "Passw(0)rd!",
    address: {
      street1: "Manzana A Casa 7",
      street2: "Villa del Campo",
      city: "Santa Marta",
      state: "Magdalena",
      country: "Colombia",
    },
    companies: [
      {
        name: "El Sitio Bacano",
        locations: [
          {
            name: "Punto de venta 1"
          }
        ],
        categories: [
          {
            name: "Adicionales",
            products: [
              {
                name: "Arepas",
                price: 3000,
                reference: "AD001",
                description: "2 Unidades",
                type: "service",
              },
              {
                name: "Bollo",
                price: 2000,
                reference: "AD002",
                description: "Unidad",
                type: "service",
              },
              {
                name: "Carne / Pechuga o Lomo De Cerdo",
                price: 7000,
                reference: "AD003",
                description: "",
                type: "service",
              },
              {
                name: "Croqueta De Yuca",
                price: 7000,
                reference: "AD004",
                description: "4 Unidades",
                type: "service",
              },
              {
                name: "Maíz",
                price: 4000,
                reference: "AD005",
                description: "",
                type: "service",
              },
              {
                name: "Papas a la francesa",
                price: 10000,
                reference: "AD006",
                description: "",
                type: "service",
              },
              {
                name: "Patacones",
                price: 10000,
                reference: "AD007",
                description: "5 Unidades",
                type: "service",
              },
              {
                name: "Porción de arroz",
                price: 10000,
                reference: "AD008",
                description: "",
                type: "service",
              },
              {
                name: "Porción de Patacones",
                price: 5000,
                reference: "AD009",
                description: "6 Unidades",
                type: "service",
              },
              {
                name: "Porción de proteina",
                price: 10000,
                reference: "AD010",
                description: "",
                type: "service",
              },
              {
                name: "Promoción De Papas a La Francesa",
                price: 6000,
                reference: "AD011",
                description: "Papas, Queso, Salsa De La Casa",
                type: "service",
              },
              {
                name: "Queso fundido",
                price: 7000,
                reference: "AD012",
                description: "",
                type: "service",
              },
              {
                name: "Queso Mozzarella",
                price: 7000,
                reference: "AD013",
                description: "",
                type: "service",
              },
              {
                name: "Sopa Adicional",
                price: 10000,
                reference: "AD014",
                description: "",
                type: "service",
              },
            ],
          },
          {
            name: "Almuerzos",
            products: [
              {
                name: "Albondigas",
                description: "",
                reference: "AL015",
                price: 12000,
                type: "service",
              },
              {
                name: "Alitas BBQQ",
                description: "",
                reference: "AL005",
                price: 10000,
                type: "service",
              },
              {
                name: "Bocachico",
                description: "",
                reference: "AL023",
                price: 15000,
                type: "service",
              },
              {
                name: "Bofe",
                description: "",
                reference: "AL001",
                price: 10000,
                type: "service",
              },
              {
                name: "Carne Asada",
                description: "",
                reference: "AL011",
                price: 12000,
                type: "service",
              },
              {
                name: "Carne Bistec",
                description: "",
                reference: "AL012",
                price: 12000,
                type: "service",
              },
              {
                name: "Carnero",
                description: "",
                reference: "AL025",
                price: 10000,
                type: "service",
              },
              {
                name: "Chuleta",
                description: "",
                reference: "AL018",
                price: 12000,
                type: "product",
              },
              {
                name: "Coginoa",
                description: "",
                reference: "AL022",
                price: 15000,
                type: "service",
              },
              {
                name: "Desechable",
                description: "",
                reference: "AL000",
                price: 1000,
                type: "service",
              },
              {
                name: "Desmechada",
                description: "",
                reference: "AL014",
                price: 12000,
                type: "service",
              },
              {
                name: "Goulash",
                description: "",
                reference: "AL024",
                price: 10000,
                type: "service",
              },
              {
                name: "H\u00edgado Bistec",
                description: "",
                reference: "AL004",
                price: 10000,
                type: "product",
              },
              {
                name: "H\u00edgado Frito",
                description: "",
                reference: "AL003",
                price: 10000,
                type: "service",
              },
              {
                name: "Lengua en Salsa",
                description: "",
                reference: "AL016",
                price: 12000,
                type: "service",
              },
              {
                name: "Lomo de cerdo",
                description: "",
                reference: "AL017",
                price: 12000,
                type: "service",
              },
              {
                name: "Milanesa de Pollo",
                description: "",
                reference: "AL009",
                price: 12000,
                type: "service",
              },
              {
                name: "Mojarra Roja",
                description: "",
                reference: "AL021",
                price: 15000,
                type: "service",
              },
              {
                name: "Mondongo",
                description: "",
                reference: "AL006",
                price: 12000,
                type: "service",
              },
              {
                name: "Nuggets de Pollo",
                description: "",
                reference: "AL010",
                price: 12000,
                type: "product",
              },
              {
                name: "Pechuga",
                description: "",
                reference: "AL019",
                price: 12000,
                type: "service",
              },
              {
                name: "Pollo Frito",
                description: "",
                reference: "AL007",
                price: 12000,
                type: "service",
              },
              {
                name: "Pollo Guisado",
                description: "",
                reference: "AL008",
                price: 12000,
                type: "product",
              },
              {
                name: "Salpic\u00f3n (Bonito)",
                description: "",
                reference: "AL020",
                price: 12000,
                type: "service",
              },
              {
                name: "Sancocho",
                description: "",
                reference: "AL026",
                price: 8000,
                type: "service",
              },
              {
                name: "Sobrebarriga",
                description: "",
                reference: "AL013",
                price: 12000,
                type: "service",
              },
              {
                name: "Ubre",
                description: "",
                reference: "AL002",
                price: 10000,
                type: "product",
              }
            ],
          },
          {
            name: "Arepas Picadas",
            products: [
              {
                name: "Arepa Picada Carne",
                description: "Más Arroz y Refresco",
                reference: "AP001",
                price: 15000,
                type: "service",
              },
              {
                name: "Arepa Picada Chory Buty",
                description: "",
                reference: "AP002",
                price: 13000,
                type: "service",
              },
              {
                name: "Arepa Picada Pollo",
                description: "",
                reference: "AP003",
                price: 14000,
                type: "service",
              },
              {
                name: "Arepa Picada Super Bacana",
                description: "",
                reference: "AP004",
                price: 22000,
                type: "service",
              },
            ],
          },
          {
            name: "Asados",
            products: [
              {
                name: "Asado Pechuga a La Plancha",
                description: "",
                reference: "AS001",
                price: 15000,
                type: "service",
              },
              {
                name: "Asado Carne a La Plancha",
                description: "",
                reference: "AS002",
                price: 16000,
                type: "service",
              },
              {
                name: "Asado Lomo De Cerdo",
                description: "",
                reference: "AS003",
                price: 17000,
                type: "service",
              },
              {
                name: "Asado Chuleta",
                description: "",
                reference: "AS004",
                price: 17000,
                type: "service",
              },
              {
                name: "Asado Super Bacano (2 Personas)",
                description: "Pechuga, Lomo De Cerdo, Carne",
                reference: "AS005",
                price: 28000,
                type: "service",
              },
            ],
          },
          {
            name: "Bebidas",
            products: [],
          },
          {
            name: "Confitería",
            products: [],
          },
          {
            name: "Desayunos",
            products: [],
          },
          {
            name: "Extra de la casa",
            products: [],
          },
          {
            name: "Hamburguesas",
            products: [],
          },
          {
            name: "Jugos Naturales",
            products: [],
          },
          {
            name: "Mazorcas desgranadas",
            products: [],
          },
          {
            name: "Patacones",
            products: [],
          },
          {
            name: "Perros Calientes",
            products: [],
          },
          {
            name: "Promociones",
            products: [
              {
                name: "4 Perros Sencillos Grandes",
                price: 12000,
                reference: "PROM001",
                description: "",
                type: "service",
              },
              {
                name: "5 Perros Sencillos Grandes",
                price: 15000,
                reference: "PROM002",
                description: "",
                type: "service",
              },
              {
                name: "Hamburguesa Carne y Pollo",
                price: 20000,
                reference: "PROM003",
                description: "Más papas a la francesa",
                type: "service",
              },
              {
                name: "Salchipapa + Perro + Hamburguesa",
                price: 25000,
                reference: "PROM004",
                description: "",
                type: "service",
              },
            ],
          },
          {
            name: "Salchipapas",
            products: [],
          },
          {
            name: "Sandwich",
            products: [],
          }
        ],
      }
    ]

  }
];

async function main() {
  console.log(`Start seeding ...`);

  data.forEach(async (user) => {
    await hash(user.password, 12).then( async (hashed_password) => {
      await prisma.user.create({
        data: {
          name: user.name,
          email: user.email,
          password: hashed_password,
        },
      }).then( async (_user) => {
        const userAddress = await prisma.address.create({
          data: {
            ...user.address,
            user: {
              connect: {
                id: _user.id,
              }
            }
          }
        });
  
        const hashed_password2 = await hash("Passw(0)rd!", 12);
        await prisma.user.create({
          data: {
            name: "Julio Cesar",
            email: "juliochemoreno@gmail.com",
            password: hashed_password2,
          },
        }).then( async (_user2) => {
          user.companies.forEach(async (company) => {
  
            await prisma.company.create({
              data: {
                name: company.name,
                // users: {
                //   connect: {
                //     id: _user.id,
                //   },
                // },
              },
            }).then( async(_company) => {
              
  
              await prisma.user.update({
                where: { id: _user.id },
                data: {
                  companies: {
                    connect: {
                      id: _company.id,
                    },
                  },
                },
              });
              
              await prisma.user.update({
                where: { id: _user2.id },
                data: {
                  companies: {
                    connect: {
                      id: _company.id,
                    },
                  },
                },
              });
  
              await prisma.userRole.create({
                data: {
                  role: UserType.admin,
                  user: {
                    connect: {
                      id: _user.id,
                    },
                  },
                  company: {
                    connect: {
                      id: _company.id,
                    },
                  },
                },
              });
        
              await prisma.userRole.create({
                data: {
                  role: UserType.manager,
                  user: {
                    connect: {
                      id: _user2.id,
                    },
                  },
                  company: {
                    connect: {
                      id: _company.id,
                    },
                  },
                },
              });
              
              const priceList = await prisma.priceList.create({
                data: {
                  name: "General",
                  status: true,
                  description: "",
                  type: "amount",
                  main: true,
                  editable: false,
                  deletable: false,
                  company: {
                    connect: {
                      id: _company.id,
                    },
                  },
                },
              });
        
              company.locations.forEach(async (location) => {
                const _location = await prisma.location.create({
                  data: {
                    name: location.name,
                    company: {
                      connect: {
                        id: _company.id,
                      },
                    },
                  }
                });
              });
        
              company.categories.forEach(async (category) => {
                const _category = await prisma.category.create({
                  data: {
                    name: category.name,
                    description: "",
                    company: {
                      connect: {
                        id: _company.id,
                      },
                    },
                  }
                });
        
                category.products.forEach(async (product) => {
                  const _product = await prisma.product.create({
                    data: {
                      name: product.name,
                      description: product.description,
                      reference: product.reference,
                      type: ProductType.service,
                      company: {
                        connect: {
                          id: _company.id,
                        },
                      },
                      category: {
                        connect: {
                          id: _category.id
                        }
                      }
                    }
                  }).then(async (_product) => {
                    await prisma.price.create({
                      data: {
                        price: product.price,
                        type: {
                          connect: {
                            id: priceList.id,
                          },
                        },
                        product: {
                          connect: {
                            id: _product.id
                          }
                        }
                      },
                    });
                  });
        
                });
              });
            }) ;
            
          });
        });
      });
    });

  });

  console.log(`Seeding finished.`);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

// sudo npx prisma migrate dev --name init
// sudo npx prisma db seed