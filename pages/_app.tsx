import { SessionProvider } from "next-auth/react";
import { AppProps } from "next/app";

import "./globals.css";
import "../styles/styles.scss";

const App = ({ Component, pageProps }) => {
  return (
    <SessionProvider session={pageProps.session}>
      <Component {...pageProps} />
    </SessionProvider>
  );
};

export default App;
