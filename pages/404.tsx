import Link from "next/link";

export default function Page404() {
  return (
    <div className="h-screen bg-base-100">
      <main className="grid min-h-full place-items-center px-6 py-24 sm:py-32 lg:px-8">
        <div className="text-center prose">
          <h2>404</h2>
          <h1>Página no encontrada</h1>
          <p>Lo sentimos, no hemos podido encontrar la página que busca.</p>
          
          <div className="mt-10 flex items-center justify-center gap-x-6">
            <Link href="/" className="btn btn-neutral">Ir al inicio</Link>
            <Link href="/" className="btn btn-neutral">Soporte técnico</Link>
          </div>
        </div>
      </main>
    </div>
  )
}
