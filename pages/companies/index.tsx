import React, { useEffect, useState } from "react";
import type { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { getSession } from 'next-auth/react';
import Layout from "../../components/Layouts/Layout";
import prisma from "../../lib/prisma";
import Link from "next/link";
import { HiEye } from "react-icons/hi";
import { User } from "@prisma/client";
import { Company } from "../../types/next-auth";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);

  if (!session || !session.user) {
    return {
      props: {
        user: null,
        companies: []
      },
    };
  }

  const userCompanies = await prisma.company.findMany({
    where: {
      users: {
        some: {
          id: session.user?.id,
        },
      },
    },
    select: {
      id: true,
      name: true,      
      description: true,
    },
  });
  
  const companies = await Promise.all(
    userCompanies.map(async (company) => {
      const userRole = await prisma.userRole.findFirst({
        where: {
          userId: session.user?.id,
          companyId: company.id,
        },
        select: {
          role: true,
          companyId: true,
        },
      });
  
      return {
        ...company,
        role: userRole.role      
      };
    })
  );
      
  return {
    props: {
      user: session.user,
      companies: companies
    },
  };
};

type Props = {
  companies: Company[];
  user: User;
};

const Blog: React.FC<Props> = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [companies, setCompanies] = useState([]);

  useEffect(() => {
    setCompanies(props.companies);
  }, []);

  const submitData = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    try {
      const body = { name, description };
      await fetch(`/api/company`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      });

      setName("");
      setDescription("");
      getAllCompanies().then(() => {
        closeModal();
      });
    } catch (error) {
      console.error(error);
    }
  };

  function showModal() {
    const dialogElement = document.getElementById('add_company_modal') as HTMLDialogElement;
    if (dialogElement) {
      dialogElement.showModal();
    }
  }
  
  function closeModal() {
    const dialogElement = document.getElementById('add_company_modal') as HTMLDialogElement;
    if (dialogElement) {
      dialogElement.close();
    }
  } 

  async function getAllCompanies() {
    fetch("/api/companies")
      .then((response) => {
        if (!response.ok) {
          throw new Error("No se pudo obtener la lista de empresas.");
        }
        return response.json();
      })
      .then((data) => {
        setCompanies(data);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  return (
    <Layout>
      <div className="navbar bg-base-300 text-base-300-content prose">
        <div className="navbar-start"></div>
        <div className="navbar-center">
          <h2 className="m-0">Mis empresas</h2>
        </div>
        <div className="navbar-end">
          <button className="btn btn-sm btn-ghost" onClick={() => showModal()}>Agregar</button>
        </div>
      </div>

      <dialog id="add_company_modal" className="modal">
        <div className="modal-box">
          <form method="dialog">
            <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">✕</button>
          </form>

          <h3 className="font-bold text-lg">Agregar empresa</h3>

          <form onSubmit={submitData} className="flex flex-col gap-4 py-3">
            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Nombre de la empresa</span>
              </label>
              <input type="text" className="input input-bordered w-full" onChange={(e) => setName(e.target.value)} value={name} />
            </div>

            <div className="form-control">
              <label className="label">
                <span className="label-text">Descripción</span>
              </label>
              <textarea className="textarea textarea-bordered h-32" onChange={(e) => setDescription(e.target.value)} value={description}></textarea>
            </div>

            <input className="btn btn-success w-full" disabled={!name} type="submit" value="Agregar" />
          </form>
        </div>
      </dialog>

      <section className="prose p-4">
        <div className="bg-base-100 overflow-x-auto border border-base-300 h-table rounded-lg">
          <table className="table table-pin-rows table-pin-cols m-0">
            <thead>
              <tr className="bg-neutral text-neutral-content">
                <td>ID</td>
                <td>Nombre</td>
                <td>Descripción</td>
                <td>Rol</td>
                <td className="w-48 text-center">Detalles</td>
              </tr>
            </thead>

            <tbody>
              {companies.length === 0 ? (
                <tr>
                  <td colSpan={5}>No hay resultados disponibles.</td>
                </tr>
              ) : (
                companies.map((company: Company) => (
                  <tr key={company.id} className="hover">
                    <td>{company.id}</td>
                    <td>{company.name ?? "N/A"}</td>
                    <td>{company.description ?? "N/A"}</td>
                    <td>{company.role ?? "N/A"}</td>
                    <td className="flex justify-center">
                      <Link className="btn btn-circle btn-ghost" href={`/companies/${company.id}`}><HiEye /></Link>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        </div>
      </section>
    </Layout>
  );
};

export default Blog;
