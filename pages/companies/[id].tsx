import React, { useEffect, useState } from "react";
import { GetServerSideProps, GetServerSidePropsContext, InferGetServerSidePropsType } from "next";
import Layout from "../../components/Layouts/Layout";
import Router from "next/router";
import prisma from "../../lib/prisma";
import { getSession, useSession } from "next-auth/react";
import { MdArrowBackIos } from "react-icons/md";
import { Address, Category, Company, Price } from "@prisma/client";
import toast, { Toaster } from "react-hot-toast";
import Notification from "../../components/Toast";
import { ParsedUrlQuery } from "querystring";
import TabContent from "../../components/TabContent";
import Link from "next/link";
import { HiEye, HiLockClosed, HiLockOpen, HiPencilAlt } from "react-icons/hi";
import { BsCurrencyDollar } from "react-icons/bs";
import { formatAddress } from "../../lib/utils";
import { useCombobox } from "downshift";
import { AiFillTag, AiOutlineClose } from "react-icons/ai";

type Params = {
  id: string;
};

type Location = {
  id: number;
  name: string;
  companyId: number;
  addressId: number;
  address: Address;
};

type Product = {
  id: number;
  name: string;
  description: string;
  companyId: number;
  orderId: number;
  categoryId: number;
  hasNoIvaDays: boolean;
  reference: string;
  status: boolean;
  type: string;
  price: [Price];
  category: Category;
}

export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext<ParsedUrlQuery>) => {
  const session = await getSession(context);

  if (!session || !session.user) {
    return {
      props: {
        isLogin: false,
        session,
        company: null,
        locations: [],
        categories: [],
        products: [],
      },
    };
  }

  const params = context.params as Params;
  const companyId = Number(params.id) || -1;

  const company = await prisma.company.findUnique({
    where: {
      id: companyId,
    },
    include: {
      users: {
        select: {
          id: true,
          name: true,
          email: true,
        },
      },
    },
  });  

  const locations = await prisma.location.findMany({
    where: {
      companyId
    },
    include: {
      address: true,
    }
  });

  const categories = await prisma.category.findMany({
    where: {
      companyId
    }   
  });

  const products = await prisma.product.findMany({
    where: {
      companyId
    }, 
    include: {
      category: true,
      price: true,
    }
  });

  return {
    props: {
      session,
      company,
      locations,
      categories,
      products,
    },
  };
};

const Post: React.FC<Company> = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { data: session, status } = useSession();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [error, setError] = useState(null);

  const [locations, setLocations] = useState([]);
  const [categories, setCategories] = useState([]);
  const [products, setProducts] = useState([]);

  const [locationName, setLocationName] = useState("");
  const [companyId, setCompanyId] = useState("");
  const defaultAddress = {
    street1: '',
    street2: '',
    city: '',
    state: '',
    country: '',
    zipCode: '',
  };
  
  const [address, setAddress] = useState(defaultAddress);
  const [category, setCategory] = useState({
    name: '',
    description: '',
  });
  
  const [product, setProduct] = useState({
    name: '',
    category: '',
    price: '',
  });

  const [items, setItems] = useState([]);  
  const {
    isOpen,
    selectedItem,
    inputValue,
    getMenuProps,
    getInputProps,
    getItemProps,
    selectItem,
  } = useCombobox({
    onInputValueChange({inputValue}) {
      setItems(categories.filter(getCategoriesFilter(inputValue)));
    },
    items,
    itemToString(item) { return item ? item.name : '' },
  });

  useEffect(() => {
    setCompanyId(props.company.id ?? "");
    setName(props.company.name ?? "");
    setDescription(props.company.description ?? "");
    setLocations(props.locations ?? []);
    setCategories(props.categories ?? []);
    setProducts(props.products ?? []);
    setItems(props.categories ?? []);

    console.log(props.products);
  }, []);

  useEffect(() => {
    if (error) {
      openToast(error, "error");
    }
  }, [error]);

  useEffect(() => {
    setProduct((prevCategory) => ({
      ...prevCategory,
      category: selectedItem && inputValue == selectedItem.name ? selectedItem : null,
    }));
  }, [inputValue, selectedItem]);

  const goBack = () => {
    Router.back();
  };

  const handleNameChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
    setName(event.target.value);
  };

  const handleDescriptionChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
    setDescription(event.target.value);
  };

  const submitData = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    try {
      const body = { name: locationName, companyId, address };
      await fetch(`/api/location`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      });

      setLocationName("");
      setAddress(defaultAddress);
      getAllLocations().then(() => {
        closeModal('add_location_modal');
        openToast("Se ha agregado un nuevo punto de venta", "success");
      });
    } catch (error) {
      console.error(error);
      openToast("Error al agregar el punto de venta", "error");
    }
  };

  const submitCategoryData = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    try {
      const body = { 
        ...category,
        companyId
      };

      await fetch(`/api/category`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      }).then(() => {
        setCategory({
          name: '',
          description: '',
        });
  
        getAllCategories().then(() => {
          closeModal('add_category_modal');
          openToast("Se ha agregado una categoría", "success");
        });

      });
    } catch (error) {
      console.error(error);
      openToast("Error al agregar la categoría", "error");
    }
  };

  const submitProductData = async (e: React.SyntheticEvent) => {
    e.preventDefault();

    try {
      const body = { 
        ...product,
        companyId,
      };

      console.log(body);

      await fetch(`/api/product`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      }).then(() => {
        setProduct({
          name: '',
          category: '',
          price: '',
        });
  
        getAllProducts().then(() => {
          closeModal('add_product_modal');
          openToast("Se ha agregado un producto", "success");
        });

      });

    } catch (error) {
      console.error(error);
      openToast("Error al agregar la categoría", "error");
    }
  };

  const openToast = (text: string, type: string) => {
    toast.custom((t) => (
      <Notification visible={t.visible} text={text} type={type} />
    ));
  }

  const showModal = (modalId: string) => {
    const modalElement = document.getElementById(modalId) as HTMLDialogElement | null;
    if (modalElement) {
      modalElement.showModal();
    }
  }
  
  const closeModal = (modalId: string) => {
    const modalElement = document.getElementById(modalId) as HTMLDialogElement | null;
    if (modalElement) {
      modalElement.close();
    }
  }
  
  async function updateCompany(id: number) {
    const updatedCompany = {
      name,
      description,
      session,
      status,
    };

    try {
      const response = await fetch(`/api/company/${id}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedCompany),
      });

      if (!response.ok) {
        throw new Error(`Error en la solicitud: ${response.status} - ${response.statusText}`);
      }

      openToast("Se han actualizado los datos de la empresa", "success");
    } catch (error) {
      console.error("Error al actualizar la empresa", error);
      openToast("Error al actualizar la empresa", "error");
    }

  }

  async function getAllLocations() {
    await fetch(`/api/locations`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ companyId }),
    })
    .then((response) => {
      if (!response.ok) {
        throw new Error("No se pudo obtener la lista de puntos de venta.");
      }
      return response.json();
    })
    .then((data) => {
      setLocations(data);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  async function getAllCategories() {    
    await fetch(`/api/categories`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ companyId }),
    }).then((response) => {
      if (!response.ok) {
        throw new Error("No se pudo obtener la lista de categorías.");
      }
      return response.json();
    })
    .then((data) => {
      setCategories(data);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  async function getAllProducts() {    
    await fetch(`/api/products`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ companyId }),
    }).then((response) => {
      if (!response.ok) {
        throw new Error("No se pudo obtener la lista de productos.");
      }
      return response.json();
    })
    .then((data) => {
      setProducts(data);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  const handleChangeAddress = (e: { target: { name: any; value: any; }; }) => {
    const { name, value } = e.target;
    setAddress((prevAddress) => ({
      ...prevAddress,
      [name]: value,
    }));
  };

  const handleChangeCategory = (e: { target: { name: any; value: any; }; }) => {
    const { name, value } = e.target;
    setCategory((prevCategory) => ({
      ...prevCategory,
      [name]: value,
    }));
  };

  const handleChangeProduct = (e) => {
    const { name, value } = e.target;
    setProduct((prevCategory) => ({
      ...prevCategory,
      [name]: value,
    }));
  };
  
  function getCategoriesFilter(inputValue: string) {
    const lowerCasedInputValue = inputValue.toLowerCase();
    return function categorysFilter(category: Category) {
      return ( !inputValue || category.name.toLowerCase().includes(lowerCasedInputValue) )
    }
  }

  return (
    <Layout>
      <div className="navbar bg-base-300 text-base-300-content prose">
        <div className="navbar-start">
          <button onClick={goBack} className="btn btn-sm btn-ghost">
            <MdArrowBackIos /> Volver
          </button>
        </div>
        <div className="navbar-center">
          <h2 className="m-0">{props.company.name}</h2>
        </div>
        <div className="navbar-end"></div>
      </div>

      <section className="flex flex-col gap-4 prose p-4 flex-1">
        <TabContent tabs={["Configuraciones", "Puntos de venta", "Categorías", "Productos"]} defaultTab={0}>
          <>
            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Nombre de la empresa</span>
              </label>
              <input
                type="text"
                className="input input-bordered w-full"
                value={name}
                onChange={handleNameChange}
              />
            </div>

            <div className="form-control">
              <label className="label">
                <span className="label-text">Descripción</span>
              </label>
              <textarea className="textarea textarea-bordered h-24" value={description} onChange={handleDescriptionChange} />
            </div>

            <button className="btn btn-active btn-primary" onClick={() => updateCompany(props.company.id)} disabled={!name || !description} >
              Guardar cambios
            </button>
          </>

          <>
            <div className="bg-base-100 overflow-x-auto border border-base-300 max-h-table rounded-lg">
              <table className="table table-pin-rows table-pin-cols m-0">
                <thead>
                  <tr className="bg-base-300 text-base-300-content">
                    <td className="w-10">ID</td>
                    <td>Nombre</td>
                    <td>Dirección</td>
                    <td className="w-48 text-center">Acciones</td>
                  </tr>
                </thead>

                <tbody>
                  { locations.length === 0 ? (
                    <tr>
                      <td colSpan={4}>Crea tu primer punto de venta fácil y rápido. 🚀</td>
                    </tr>
                  ) : (
                    locations.map((location: Location, index) => (
                      <tr key={location.id} className="hover">
                        <td>{index + 1}</td>
                        <td>{location.name ?? "N/A"}</td>
                        <td>{location.address && formatAddress(location.address)}</td>
                        <td className="flex justify-center">
                          <Link className="btn btn-sm btn-ghost" href={`/companies/${companyId}/location/${location.id}`}><HiEye className="h-4 w-4"/></Link>
                          <button className="btn btn-sm btn-ghost"><HiPencilAlt className="h-4 w-4"/></button>
                          <button className="btn btn-sm btn-ghost"><HiLockOpen className="h-4 w-4"/></button>
                        </td>
                      </tr>
                    ))
                  )}
                </tbody>
              </table>
            </div>
            <button className="btn btn-primary" onClick={() => showModal('add_location_modal')}>Agregar punto de venta</button>
          </>

          <>
            <div className="bg-base-100 overflow-x-auto border border-base-300 max-h-table rounded-lg">
              <table className="table table-pin-rows table-pin-cols m-0">
                <thead>
                  <tr className="bg-base-300 text-base-300-content">
                    <td className="w-10">ID</td>
                    <td>Nombre</td>
                    <td>Descripción</td>
                    <td className="w-48 text-center">Acciones</td>
                  </tr>
                </thead>
                
                <tbody>
                  {categories.length === 0 ? (
                      <tr>
                        <td colSpan={4}>
                          <p className="text-center">Crea tu primera categoría fácil y rápido. 🚀</p>
                        </td>
                      </tr>
                    ) : (
                      categories.map((category: Category, index) => (
                        <tr key={category.id} className="hover">
                          <td>{index + 1}</td>
                          <td>{category.name ?? "N/A"}</td>
                          <td>{category.description}</td>
                          <td className="flex justify-center">
                            <button className="btn btn-sm btn-ghost"><HiEye className="h-4 w-4"/></button>
                            <button className="btn btn-sm btn-ghost"><HiPencilAlt className="h-4 w-4"/></button>
                            <button className="btn btn-sm btn-ghost"><HiLockClosed className="h-4 w-4"/></button>
                          </td>
                        </tr>
                      ))
                    )}
                </tbody>
              </table>
            </div>
            <button className="btn btn-primary" onClick={() => showModal('add_category_modal')}>Agregar categoría</button>

          </>

          <>
            <div className="bg-base-100 overflow-x-auto border border-base-300 max-h-table rounded-lg">
              <table className="table table-pin-rows table-pin-cols m-0">
                <thead>
                  <tr className="bg-base-300 text-base-300-content">
                    <td className="w-10">ID</td>
                    <td>Nombre</td>
                    <td>Referencia</td>
                    <td>Categoría</td>
                    <td>Precio</td>
                    <td className="w-48 text-center">Acciones</td>
                  </tr>
                </thead>
                
                <tbody>
                  {products.length === 0 ? (
                      <tr>
                        <td colSpan={4}>
                          <p className="text-center">Crea tu primer producto fácil y rápido. 🚀</p>
                        </td>
                      </tr>
                    ) : (
                      products.map((product: Product, index) => (
                        <tr key={product.id} className="hover">
                          <td>{index + 1}</td>
                          <td>
                            <span className="label-text">{product.name}</span><br/>
                            <span className="label-text-alt">{product.description}</span>
                          </td>
                          <td>{product.reference}</td>
                          <td>{product.category.name}</td>
                          <td>{product.price[0].price}</td>
                          <td className="flex justify-center">
                            <button className="btn btn-sm btn-ghost"><HiEye className="h-4 w-4"/></button>
                            <button className="btn btn-sm btn-ghost"><HiPencilAlt className="h-4 w-4"/></button>
                            <button className="btn btn-sm btn-ghost"><HiLockClosed className="h-4 w-4"/></button>
                          </td>
                        </tr>
                      ))
                    )}
                </tbody>
              </table>
            </div>
            <button className="btn btn-primary" onClick={() => showModal('add_product_modal')}>Agregar producto</button>

          </>



        </TabContent>
      </section>

      <dialog id="add_location_modal" className="modal">
        <div className="modal-box w-11/12 max-w-5xl">
          <form method="dialog">
            <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">✕</button>
          </form>

          <h2 className="font-bold text-xl">Agregar punto de venta</h2>
          <div className="divider"></div>

          <form onSubmit={submitData} className="flex flex-col gap-4">
            <div className="flex gap-4">
              <div className="form-control w-1/2">
                <label className="label">
                  <span className="label-text">Nombre</span>
                </label>
                <input type="text" className="input input-bordered w-full" onChange={(e) => setLocationName(e.target.value)} value={locationName} />
              </div>

              <div className="form-control w-1/2">
                <label className="label">
                  <span className="label-text">Dirección</span>
                </label>
                <input type="text" className="input input-bordered w-full" name="street1" value={address.street1} onChange={handleChangeAddress} />
              </div>
            </div>

            <div className="flex gap-4">
              <div className="form-control w-1/2">
                <label className="label">
                  <span className="label-text">Ciudad</span>
                </label>
                <input type="text" className="input input-bordered w-full" name="city" value={address.city} onChange={handleChangeAddress} />
              </div>

              <div className="form-control w-1/2">
                <label className="label">
                  <span className="label-text">Departamento</span>
                </label>
                <input type="text" className="input input-bordered w-full" name="state" value={address.state} onChange={handleChangeAddress} />
              </div>

              <div className="form-control w-1/2">
                <label className="label">
                  <span className="label-text">Código postal</span>
                </label>
                <input type="text" className="input input-bordered w-full" name="zipCode" value={address.zipCode} onChange={handleChangeAddress} />
              </div>

              <div className="form-control w-1/2">
                <label className="label">
                  <span className="label-text">País</span>
                </label>
                <input type="text" className="input input-bordered w-full" name="country" value={address.country} onChange={handleChangeAddress} />
              </div>
            </div>

            <input className="btn btn-success w-full" disabled={!locationName} type="submit" value="Agregar" />
          </form>
        </div>
      </dialog>

      <dialog id="add_category_modal" className="modal">
        <div className="modal-box">
          <form method="dialog">
            <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">✕</button>
          </form>

          <h2 className="font-bold text-xl">Agregar categoría</h2>
          <div className="divider"></div>

          <form onSubmit={submitCategoryData} className="flex flex-col gap-4">
            <div className="flex flex-col gap-4">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text">Nombre</span>
                </label>
                <input type="text" className="input input-bordered w-full" name="name" value={category.name} onChange={handleChangeCategory}/>
              </div>

              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text">Descripción</span>
                </label>
                <input type="text" className="input input-bordered w-full" name="description" value={category.description} onChange={handleChangeCategory} />
              </div>
            </div>

            <input className="btn btn-success w-full" disabled={!category.name} type="submit" value="Agregar" />
          </form>
        </div>
      </dialog>

      <dialog id="add_product_modal" className="modal">
        <div className="modal-box">
          <form method="dialog">
            <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">✕</button>
          </form>

          <h2 className="font-bold text-xl">Agregar producto</h2>
          <div className="divider"></div>

          <form onSubmit={submitProductData} className="flex flex-col gap-4">
            <div className="flex flex-col gap-4">
              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text">Nombre</span>
                </label>
                <input className="input input-bordered w-full" name="name" value={product.name} onChange={handleChangeProduct}/>
              </div>

              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text">Categoría</span>
                </label>

                <div className="relative w-full flex flex-col">

                  <div className="join w-full">
                    <div className="join-item input input-bordered flex items-center">
                      <AiFillTag/>
                    </div>

                    <input placeholder="Buscar categoria" className="w-full input input-bordered join-item" {...getInputProps()} />

                    { selectedItem &&
                      <button aria-label="Limpiar búsqueda" className="input input-bordered flex items-center join-item" type="button" onClick={() => { selectItem(null); }} >
                        <AiOutlineClose/>
                      </button>
                    }

                    {/* <button aria-label="Toggle menu" className="input input-bordered flex items-center join-item" type="button" {...getToggleButtonProps()}>
                      {isOpen ? <AiOutlineCaretUp /> : <AiOutlineCaretDown />}
                    </button> */}
                  </div>

                  <div
                    className={`absolute top-[3rem] mt-1 flex-1 w-full overflow-hidden rounded-md z-10 shadow-md ${!(isOpen) && 'hidden'}`}
                    {...getMenuProps()}
                  >
                    <div className="max-h-32 overflow-y-auto">
                      <ul {...getMenuProps()} className="menu bg-base-200">
                        {items.length > 0 ? (
                          items.map((item, index) => (
                            <li key={item.id} {...getItemProps({ item, index })}>
                              <a>{item.name}</a>
                            </li>
                          ))
                        ) : (
                          <li className="disabled">
                            <a>No se encontraron elementos</a>
                          </li>
                        )}
                      </ul>
                    </div>
                  </div>
                </div>
             
              </div>

              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text">Precio</span>
                </label>

                <div className="join w-full">
                  <div className="join-item input input-bordered flex items-center">
                    <BsCurrencyDollar />
                  </div>
                  <input className="join-item input input-bordered w-full" name="price" value={product.price} onChange={handleChangeProduct}/>
                </div>
              </div>

              
            </div>

            <input className="btn btn-success w-full" disabled={!product.name || !product.price} type="submit" value="Agregar" />
          </form>
        </div>
      </dialog>

      <Toaster position="top-center" reverseOrder={false} />
    </Layout>
  );
};

export default Post;
