"use client";

import { signIn } from "next-auth/react";
import { useSearchParams, useRouter } from "next/navigation";
import { useState } from "react";
import AuthLayout from "../../components/Layouts/LayoutAuth";
import Link from "next/link";
import { FcGoogle } from "react-icons/fc";
import Image from "next/image";
import logoColor from "/images/logos/tayrosoft-logo.svg";
import logoWhite from "/images/logos/tayrosoft-logo-white.svg";

export default function LoginPage() {
  const router = useRouter();

  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const searchParams = useSearchParams();
  const callbackUrl = searchParams.get("callbackUrl") || "/";

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      setLoading(true);

      const res = await signIn("credentials", {
        redirect: false,
        email: email,
        password: password,
        callbackUrl,
      });

      if (!res?.error) {
        router.push(callbackUrl);
      } else {
        setLoading(false);
        setError("Correo electrónico o contraseña no válidos");
      }
    } catch (error: any) {
      setLoading(false);
      setError(error);
    }
  };

  return (
    <AuthLayout>
      <section className="flex justify-center items-center m-auto p-3">
        <form className="card card-compact w-96 bg-base-300 shadow-xl p-3" onSubmit={handleSubmit}>
          <div className="card-body">            
            <Image src={logoWhite} alt="Tayrosoft Logo" className="h-12"/>

            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Correo electrónico</span>
              </label>
              <input required type="email" placeholder="example@mail.com" className="input input-bordered w-full" onChange={(e) => { setEmail(e.target.value); }} />
            </div>

            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Contraseña</span>
              </label>
              <input required type="password" placeholder="********" className="input input-bordered w-full" onChange={(e) => { setPassword(e.target.value); }} />
            </div>

            <button className="btn btn-block btn-primary mt-3" disabled={loading}>
              {loading && <span className="loading loading-spinner"></span>}
              Iniciar sesión
            </button>
            
            <div className="divider">O</div>

            <a className="btn btn-block btn-neutral" onClick={() => signIn("google", { callbackUrl })} role="button">
              <FcGoogle/>
              Continuar con Google
            </a>

            {error && <div className="alert alert-warning mb-3">
              <svg xmlns="http://www.w3.org/2000/svg" className="stroke-current shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" /></svg>
              <span>{error}</span>
            </div>}

            <label className="label">
              <span className="label-text-alt">¿Aún no tiene cuenta?</span>
              <span className="label-text-alt"><Link href="/auth/register" className="link link-hover">Registrarse</Link></span>
            </label>
          </div>
        </form>
      </section>
    </AuthLayout>
  );
}
