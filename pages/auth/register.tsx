import React, { useEffect, useState } from "react";
import AuthLayout from "../../components/Layouts/LayoutAuth";
import type { GetServerSidePropsContext, InferGetServerSidePropsType } from "next";
import { getProviders, signIn } from "next-auth/react";
import { getServerSession } from "next-auth/next";
import { authOptions } from "../api/auth/[...nextauth]";
import Link from "next/link";
import logoColor from "/images/logos/tayrosoft-logo.svg";
import logoWhite from "/images/logos/tayrosoft-logo-white.svg";
import Image from "next/image";

export default function Register({ providers }: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);
  const [passError, setPassError] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    validatePassword(password, confirmPassword);
  }, [password, confirmPassword]);

  function validatePassword(pass, confirmPass) {
    let isValid = confirmPass == pass;
    setPassError(!isValid);
  }

  async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);

    let userData = { name, email, password };

    try {
      const res = await fetch("/api/auth/register", {
        method: "POST",
        body: JSON.stringify(userData),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (res.ok) {
        const data = await res.json();
        let resSignIn = await signIn("credentials", {
          email,
          password,
          callbackUrl: "/",
          redirect: true,
        });

        if (resSignIn?.ok) {
          return;
        } else {
          setError("Error. Compruebe sus datos e inténtelo de nuevo.");
          console.log("Failed", res);
        }

        setLoading(false);
      } else {
        const errorData = await res.json();
        console.error("Error en el registro:", errorData.errors);
        setError("Error en el registro: " + errorData.errors);
        setLoading(false);
      }
    } catch (error) {
      console.error("Error inesperado:", error);
      setError("Error inesperado:" + error);
      setLoading(false);
    }
  }

  return (
    <AuthLayout>
      <div className="flex justify-center items-center m-auto p-4">
        <form className="card card-compact w-96 bg-base-300 shadow-xl p-3" onSubmit={handleSubmit}>
          <Image src={logoWhite} alt="Tayrosoft Logo" className="h-12"/>

          <div className="card-body">
            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Nombre</span>
              </label>
              <input required type="text" placeholder="Nombre" className="input input-bordered w-full" onChange={(e) => { setName(e.target.value); }} />
            </div>

            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Correo electrónico</span>
              </label>
              <input required type="email" placeholder="example@mail.com" className="input input-bordered w-full" onChange={(e) => { setEmail(e.target.value); }} />
            </div>

            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Contraseña</span>
              </label>
              <input required type="password" placeholder="********" className="input input-bordered w-full" onChange={(e) => { setPassword(e.target.value); }} />
            </div>

            <div className="form-control w-full">
              <label className="label">
                <span className="label-text">Confirmar Contraseña</span>
              </label>
              <input required type="password" placeholder="********" className="input input-bordered w-full" onChange={(e) => { setConfirmPassword(e.target.value); }} />
              {passError && confirmPassword && password && <label className="label">
                <span className="label-text-alt text-error">Las contraseñas no coinciden</span>
              </label>}
            </div>

            <button className="btn btn-block btn-primary my-3" disabled={ passError || loading || !name || !email || !password || !confirmPassword }>
              {loading && <span className="loading loading-spinner"></span>}
              Regístrarse
            </button>

            {error && <div className="alert alert-warning mb-3">
              <svg xmlns="http://www.w3.org/2000/svg" className="stroke-current shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" /></svg>
              <span>{error}</span>
            </div>}

            <label className="label">
              <span className="label-text-alt">¿Ya tienes una cuenta? </span>
              <span className="label-text-alt"><Link href="/auth/login" className="link link-hover">Iniciar sesión</Link></span>
            </label>
          </div>
        </form>
      </div>
    </AuthLayout>
  )
}

export async function getServerSideProps(context: GetServerSidePropsContext) {
  const session = await getServerSession(context.req, context.res, authOptions);

  // If the user is already logged in, redirect.
  // Note: Make sure not to redirect to the same page
  // To avoid an infinite loop!
  if (session) {
    return { redirect: { destination: "/" } };
  }

  const providers = await getProviders();

  return {
    props: { providers: providers ?? [] },
  }
}