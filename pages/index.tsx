import React, { useEffect, useState } from "react";
import type { GetServerSideProps, InferGetServerSidePropsType } from "next";
import Layout from "../components/Layouts/Layout";
import { getSession, useSession } from 'next-auth/react';
import { Card } from 'flowbite-react';
import prisma from "../lib/prisma";
import { Category, Company, Price } from "@prisma/client";
import Link from "next/link";
import { HiCheck, HiEye, HiMinus, HiOutlinePlus, HiPencil, HiPrinter, HiSearch, HiTrash, HiUserAdd } from "react-icons/hi";
import { MdTableRestaurant } from "react-icons/md";
import { AiOutlineBarcode } from "react-icons/ai";
import { FaBarcode } from "react-icons/fa6";
import { Product } from "../types/next-auth";

const tables = [
  'Mesa 20',  
  'Mesa 21',  
  'Mesa 22',  
  'Mesa 23',  
  'Mesa 24',  
  'Mesa 25',  
  'Mesa 26',  
  'Mesa 27',  
  'Mesa 28',  
  'Mesa 29',  
  'Mesa 30',
];

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);

  if (!session || !session.user) {
    return {
      props: {
        isLogin: false
      },
    };
  }

  return {
    props: {
      isLogin: true,
      user: session.user,
    },
  };
};

type Props = {
  isLogin: Boolean;
  user: {
    name?: string;
    email?: string;
    image?: string;
  };
};

const Home: React.FC<Props> = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { data: session, status } = useSession();
  const [companyId, setCompanyId] = useState("");
  const [locationId, setLocationId] = useState("");
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);

  const [products, setProducts] = useState([]);

  const [selectedTable, setSelectedTable] = useState(tables[0]);
  const [query, setQuery] = useState('');

  const [cliente, setCliente] = useState(1);
  const [mesa, setMesa] = useState(1);

  const handleClienteChange = (event) => {
    setCliente(event.target.value);
  };

  const handleMesaChange = (event) => {
    setMesa(event.target.value);
  };


  const filteredTables =
    query === ''
      ? tables
      : tables.filter((table) => {
          return table.toLowerCase().includes(query.toLowerCase())
        });

  useEffect(() => {
    const savedCompanyId = localStorage.getItem("selectedCompany");
    if (savedCompanyId) {
      setCompanyId(savedCompanyId);
      getAllCategories(Number(savedCompanyId));
      getAllProducts(Number(savedCompanyId))
    }

    const savedLocationId = localStorage.getItem("selectedLocation");
    if (savedLocationId) {
      setLocationId(savedLocationId);
    }
    
  }, []);

  async function getAllCategories(companyId: number) {    
    await fetch(`/api/categories`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ companyId }),
    }).then((response) => {
      if (!response.ok) {
        throw new Error("No se pudo obtener la lista de categorías.");
      }
      return response.json();
    })
    .then((data) => {
      setCategories(data);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  async function getAllProducts(companyId: number) {    
    await fetch(`/api/products`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ companyId }),
    }).then((response) => {
      if (!response.ok) {
        throw new Error("No se pudo obtener la lista de productos.");
      }
      return response.json();
    })
    .then((data) => {
      setProducts(data);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  function handleSetSelectedCategory(categoryId: number) {
    setSelectedCategory(selectedCategory != categoryId ? categoryId : "" );
  }

  const handleClick = (table: React.SetStateAction<string>) => {
    const elem = document.activeElement as HTMLElement;
    if (elem) {
      elem.blur();
    }
  
    setSelectedTable(table);
    setQuery("");
  };  

  return (
    <Layout>      
      <div className="fixed flex flex-col gap-3 p-4 bg-base-100 h-default z-20 w-16 hover:w-auto overflow-hidden hover:overflow-y-auto border-r">
        { categories.map((category: Category, index) => (
          <div key={index} className="flex items-center cursor-pointer gap-4 group" onClick={() => { handleSetSelectedCategory(category.id)  }}>
            <button className={ selectedCategory == category.id ? "btn btn-sm btn-circle btn-accent" : "btn btn-sm btn-circle group-hover:btn-primary"}>
              <p className="text-xs">{category.name.substring(0, 2).toUpperCase()}</p>
            </button>
            <p className={ selectedCategory == category.id ? "whitespace-nowrap text-accent text-xs" : "whitespace-nowrap text-xs group-hover:text-primary"}>{category.name}</p>
          </div>
        )) }        
      </div>
      
      <div className="flex pl-16 h-default z-10">
        <div className="block flex-1">
          <div className="join w-full p-4">
            <div className="join-item input input-bordered flex align-middle items-center">
              <HiSearch className="w-6 h-6"/>
            </div>

            <div className="join-item input input-bordered flex align-middle items-center">
              <FaBarcode className="w-6 h-6"/>
            </div>

            <input className="input input-bordered join-item w-full" placeholder="Buscar productos"/>
          </div>          

          <div className="h-[calc(100vh-12rem-2px)] py-4 overflow-hidden">
            <div className="grid sm:grid-col-1 md:grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4 gap-3 px-4 h-full w-full overflow-y-scroll">
              { products.map((product: Product, index) => (
                <div key={index} className="card card-compact shadow-sm bg-base-100 text-base-100-content h-full">
                  {/* <div className="indicator w-full">
                    <span className="indicator-item grid w-6 h-6 place-items-center bg-primary text-primary-content rounded-full">1</span>   
                  </div> */}
                  <div className="card-body text-center prose flex items-center justify-center">
                    <h3 className="m-0">{product.name}</h3>
                    <span className="label-text-alt">{product.reference}</span>
                    <span className="label-text">${product.price[0].price}</span>
                  </div>
                </div>
              )) }
            </div>
          </div>

          <div className="flex justify-between bg-base-100 w-full border-t">
            <button className="btn bg-base-100 rounded-none"><HiOutlinePlus className="w-4 h-4"/></button>

            <div className="join rounded-none">
              <h2 className="join-item bg-base-100 text-base-100-content flex align-middle items-center px-4">TICKECTS ABIERTOS: </h2>
              <div className="dropdown dropdown-hover dropdown-top dropdown-end join-item">
                <label tabIndex={0} className="btn btn-primary join-item"><MdTableRestaurant className="w-5 h-5" />{selectedTable}</label>
                <ul tabIndex={0} className="dropdown-content z-[1] menu p-2 shadow bg-base-100 w-48 m-2">                  
                  <input className="input input-bordered w-full" placeholder="Buscar mesa" value={query} onChange={(e) => setQuery(e.target.value) }/>
                  <div className="divider"></div>
                  {filteredTables.map((table) => (
                    <li className={table == selectedTable ? "bg-primary text-primary-content rounded" : ""} key={table} onClick={() => { handleClick(table) } } ><a>{table}</a></li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
        
        <div className="w-[35rem] border-l">
          <div className="flex justify-between items-center w-full border-b px-4 bg-base-100 prose">
            <h3 className="m-0">Factura de venta</h3>
            <button className="btn btn-circle btn-ghost"><HiPrinter className="w-6 h-6"/></button>
          </div>

          <div className="grid grid-cols-3 gap-4 bg-base-100 border-b p-4">
            <div className="form-control w-full col-span-2">
              <label className="label">
                <span className="label-text">Cliente</span>
              </label>
              <select className="select select-bordered" value={cliente} onChange={handleClienteChange}>
                <option value="1">Cliente 1</option>
                <option value="2">Cliente 2</option>
              </select>
            </div>
            
            <div className="form-control w-full col-span-1">
              <label className="label">
                <span className="label-text">Mesa</span>
              </label>
              <select className="select select-bordered" value={mesa} onChange={handleMesaChange}>
                <option value="1">Mesa 1</option>
                <option value="2">Mesa 2</option>
              </select>
            </div>
          </div>
          
          <div className="h-[calc(100vh-17.5rem)] overflow-auto">
            <div className="grid grid-cols-4 items-center gap-4 bg-base-100 border-b p-4 group cursor-pointer">
              <div className="col-span-2">
                <h2>Producto 1</h2>
                <p className="text-sm">$10.000 | REF123</p>
              </div>

              <div className="join col-span-1">
                <button className="btn btn-sm join-item"><HiMinus className="w-4 h-4"/></button>
                <input type="text" className="input input-sm join-item w-10 text-center" value="2" disabled/>
                <button className="btn btn-sm join-item"><HiOutlinePlus className="w-4 h-4"/></button>
              </div>

              <div className="flex justify-end col-span-1">
                <h3 className="block group-hover:hidden">$20.000</h3>

                <div className="join col-span-1 hidden group-hover:block">
                  <button className="btn btn-sm btn-warning join-item"><HiPencil className="w-4 h-4"/></button>                  
                  <button className="btn btn-sm btn-error join-item"><HiTrash className="w-4 h-4"/></button>
                </div>
              </div>
            </div>
            <div className="grid grid-cols-4 items-center gap-4 bg-base-100 border-b p-4 group cursor-pointer">
              <div className="col-span-2">
                <h2>Producto 2</h2>
                <p className="text-sm">$10.000 | REF123</p>
              </div>

              <div className="join col-span-1">
                <button className="btn btn-sm join-item"><HiMinus className="w-4 h-4"/></button>
                <input type="text" className="input input-sm join-item w-10 text-center" value="2" disabled/>
                <button className="btn btn-sm join-item"><HiOutlinePlus className="w-4 h-4"/></button>
              </div>

              <div className="flex justify-end col-span-1">
                <h3 className="block group-hover:hidden">$20.000</h3>

                <div className="join col-span-1 hidden group-hover:block">
                  <button className="btn btn-sm btn-warning join-item"><HiPencil className="w-4 h-4"/></button>                  
                  <button className="btn btn-sm btn-error join-item"><HiTrash className="w-4 h-4"/></button>
                </div>
              </div>
            </div>
          </div>

          <div className="flex">
            <button className="btn btn-success w-1/2 rounded-none">Vender</button>
            <button className="btn btn-error w-1/2 rounded-none">Cancelar</button>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Home;
