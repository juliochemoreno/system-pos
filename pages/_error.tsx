import Link from "next/link";

function Error({ statusCode }) {
  return (
    
    <div className="h-screen bg-base-100">
    <main className="grid min-h-full place-items-center px-6 py-24 sm:py-32 lg:px-8">
      <div className="text-center prose">
        <h2>{statusCode}</h2>
        <h1>Página no encontrada</h1>
        <p>{ statusCode ? `Se ha producido un error ${statusCode} en el servidor` : 'Se ha producido un error en el cliente'}</p>        
        <div className="mt-10 flex items-center justify-center gap-x-6">
          <Link href="/" className="btn btn-neutral">Ir al inicio</Link>
          <Link href="/" className="btn btn-neutral">Soporte técnico</Link>
        </div>
      </div>
    </main>
  </div>
  )
}
 
Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
} 
export default Error
