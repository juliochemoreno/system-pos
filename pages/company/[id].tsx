import React from "react";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import ReactMarkdown from "react-markdown";
import Layout from "../../components/Layouts/Layout";
import Router from "next/router";
import prisma from '../../lib/prisma'
import { useSession } from "next-auth/react";
import Link from "next/link";
import { MdArrowBackIos } from "react-icons/md";

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const company = await prisma.company.findUnique({
    where: {
      id: Number(params?.id) || -1,
    },
    include: {
      users: {
        select: {
          id: true,
          name: true,
          email: true,
        },
      },
    },
  });
  
  return {
    props: company,
  };
};

async function deletePost(id: number): Promise<void> {
  await fetch(`/api/company/${id}`, {
    method: "DELETE",
  });
  await Router.push("/companies")
}

const Company: React.FC = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { data: session, status } = useSession();
  if (status === 'loading') {
    return <div>Authenticating ...</div>;
  }
  const userHasValidSession = Boolean(session);
  const postBelongsToUser = session?.user?.email === props.user?.email;

  return (
    <Layout>
      <div className="navbar bg-base-300 text-base-300-content prose">
        <div className="navbar-start">
          <Link href="/companies" className="btn btn-sm btn-ghost"><MdArrowBackIos/> Volver</Link>
        </div>
        <div className="navbar-center">
          <h2 className="m-0">{props.name}</h2>
        </div>
        <div className="navbar-end"></div>  
      </div>

      <section className="prose p-4">
        <div>
          { props.description && <p>{props.description}</p> }

          { userHasValidSession && postBelongsToUser && (
            <button onClick={() => deletePost(props.id)}>Delete</button>
          )}
        </div>
      </section>

    </Layout>
  );
};

export default Company;
