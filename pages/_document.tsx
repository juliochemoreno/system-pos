import { Html, Head, Main, NextScript } from 'next/document'
import { useTheme } from '../lib/themes/isDarkMode';
 
export default function Document() {
  const [theme, handleChange] = useTheme();

  return (
    <Html lang="es">
      <Head />
      <body data-theme={theme}>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}