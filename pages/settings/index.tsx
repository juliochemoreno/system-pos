import React, { useEffect, useState } from "react";
import type { GetServerSideProps, InferGetServerSidePropsType } from "next";
import Layout from "../../components/Layouts/Layout";
import { getSession } from 'next-auth/react';
import prisma from "../../lib/prisma";
import { Company, User } from "@prisma/client";
import toast, { Toaster } from "react-hot-toast";
import Notification from "../../components/Toast";
import { type } from "os";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);

  if (!session || !session.user) {
    return {
      props: { 
        user: null,
        companies: [] 
      },
    };
  }

  const companies = await prisma.company.findMany({
    where: {
      users: {
        some: {
          id: session.user?.id,
        },
      },
    },
    include: {
      users: {
        select: {
          id: true,
          name: true,
        },
      },
    },
  });
  

  return {
    props: {  
      user: session.user,
      companies: companies
    },
  };
};

type Props = {
  companies: Company[];
  user: User;
};

const Settings: React.FC<Props> = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  
  const [companyId, setCompanyId] = useState("");
  const [companies, setCompanies] = useState([]);
  const [locations, setLocations] = useState([]);
  const [locationId, setLocationId] = useState("");

  useEffect(() => {
    setCompanies(props.companies);

    const savedCompanyId = localStorage.getItem("selectedCompany");
    if (savedCompanyId) {
      setCompanyId(savedCompanyId);
      getAllLocations(Number(savedCompanyId));
    }

    const savedLocationId = localStorage.getItem("selectedLocation");
    if (savedLocationId) {
      setLocationId(savedLocationId);
    }
    
  }, []);

  const submitData = async (e: React.SyntheticEvent) => {
    e.preventDefault();

    localStorage.setItem("selectedCompany", companyId);
    localStorage.setItem("selectedLocation", locationId);
    openToast("Las configuraciones se ha guardado correctamente", "success");
  }

  async function getAllLocations(companyId: number) {
    await fetch(`/api/locations`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ companyId }),
    })
    .then((response) => {
      if (!response.ok) {
        throw new Error("No se pudo obtener la lista de puntos de venta.");
      }
      return response.json();
    })
    .then((data) => {
      setLocations(data);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  const handleCompanyChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedCompanyId = e.target.value;
    setCompanyId(selectedCompanyId);
    getAllLocations(Number(selectedCompanyId));
  };

  const handleLocationChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedLocationId = e.target.value;
    setLocationId(selectedLocationId);
  };
  
  const openToast = (text: string, type: string) => {
    toast.custom((t) => (
      <Notification visible={t.visible} text={text} type={type} />
    ));
  }

  return (
    <Layout>
      <div className="navbar bg-base-300 text-base-300-content prose">
        <div className="navbar-start"></div>
        <div className="navbar-center">
          <h2 className="m-0">Configuraciones</h2>
        </div>
        <div className="navbar-end"></div>  
      </div>

      <section className="prose p-4">

        
        <form onSubmit={submitData} className="flex flex-col gap-4 py-3">

            <div className="grid grid-cols-2 gap-4">

              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text">Empresa actual</span>
                </label>

                {companies.length === 0 ? (
                  <select className="select select-warning w-full" disabled>
                    <option value="">Click para selccionar</option>
                  </select>
                ) : (
                  <select className="select select-bordered" onChange={handleCompanyChange} value={companyId}>
                    <option disabled value="">
                      Click para seleccionar
                    </option>
                    {companies.map((company: Company) => (
                      <option key={company.id} value={company.id}>
                        {company.name}
                      </option>
                    ))}
                  </select>
                )}
                
                { companies.length === 0 && <label className="label">
                  <span className="label-text text-warning">Aún no ha agregado ninguna empresa</span>
                </label> }
              </div>

              <div className="form-control w-full">
                <label className="label">
                  <span className="label-text">Punto de venta actual</span>
                </label>
                
                { locations.length === 0 ? (
                  <select className="select select-warning w-full" disabled>
                    <option value="">Click para selccionar</option>
                  </select>
                ) : (
                  <select className="select select-bordered" onChange={handleLocationChange} value={locationId}>
                    <option disabled value="">
                      Click para seleccionar
                    </option>
                    {locations.map((company: Company) => (
                      <option key={company.id} value={company.id}>
                        {company.name}
                      </option>
                    ))}
                  </select>
                )}

                { locations.length === 0 && <label className="label">
                  <span className="label-text text-warning">La empresa seleccionada no tiene ningun punto de venta</span>
                </label> }
              </div>
          </div>

          <input className="btn btn-primary w-full" type="submit" value="Guardar cambios" />
        </form>
      </section>

      <Toaster position="top-center" reverseOrder={false} />

    </Layout>
  );
};

export default Settings;
