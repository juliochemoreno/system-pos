import { Session, getServerSession } from 'next-auth';
import prisma from '../../../lib/prisma';
import { NextApiRequest, NextApiResponse } from 'next';
import { authOptions } from '../auth/[...nextauth]';

export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  try {
    const companyId = req.query.id;
    const session = await getServerSession(req, res, authOptions);

    if (req.method === 'GET') {
      await handleGetRequest(companyId, res);
    } else if (req.method === 'POST') {
      await handlePostRequest(session, companyId, req.body, res);
    } else if (req.method === 'DELETE') {
      await handleDeleteRequest(session, companyId, res);
    } else {
      res.status(405).json({ message: `Método ${req.method} no permitido` });
    }
  } catch (error) {
    console.error("Error en la solicitud:", error);
    res.status(500).json({ message: 'Error en la solicitud' });
  }
}

async function handleGetRequest(companyId: string | string[], res: NextApiResponse) {
  const company = await prisma.company.findUnique({
    where: { id: Number(companyId) },
  });
  if (!company) {
    res.status(404).json({ message: 'Empresa no encontrada' });
  } else {
    res.json(company);
  }
}

async function handlePostRequest(session: Session, companyId: string | string[], data: { name: any; description: any; }, res: NextApiResponse) {
  if (!session) {
    res.status(401).json({ message: 'Sin autorización' });
    return;
  }
  
  const { name, description } = data;
  if (!name || !description) {
    res.status(400).json({ message: 'Faltan campos requeridos' });
    return;
  }

  const updatedCompany = await prisma.company.update({
    where: { id: Number(companyId) },
    data: {
      name,
      description,
    },
  });
  res.json(updatedCompany);
}

async function handleDeleteRequest(session: Session, companyId: string | string[], res: NextApiResponse) {
  if (!session) {
    res.status(401).json({ message: 'Sin autorización' });
    return;
  }

  const company = await prisma.company.delete({
    where: { id: Number(companyId) },
  });
  res.json(company);
}
