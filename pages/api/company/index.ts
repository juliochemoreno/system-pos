import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/prisma'
import { authOptions } from '../auth/[...nextauth]'
import { getServerSession } from "next-auth/next"
import { UserType } from '@prisma/client';

// POST /api/company
// Required fields in body: name
export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  const { name, description } = req.body;

  const session = await getServerSession(req, res, authOptions)
  
  if (!session) {
    res.status(401).json({ message: "Debes haber iniciado sesión." });
    return;
  }

  const userId = session.user?.id;
  const result = await prisma.company.create({
    data: {
      name: name,
      description: description,
      users: {
        connect: {
          id: userId,
        },
      },
    },
  }).then( async (company) => {
    await prisma.userRole.create({
      data: {
        role: UserType.admin,
        user: {
          connect: {
            id: userId,
          },
        },
        company: {
          connect: {
            id: company.id,
          },
        },
      },
    });
  });
  
  return res.json(result);
}
