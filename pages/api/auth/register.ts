import { hash } from "bcryptjs";
import prisma from "../../../lib/prisma";
import { Prisma } from "@prisma/client";

export default async function handle(req, res) {
  if (req.method === "POST") {
    // create user
    await createUserHandler(req, res);
  } else {
    return res.status(405).json({ message: "Método no permitido" });
  }
}

async function createUserHandler(req, res) {
  let errors = [];
  const { name, email, password } = req.body;

  if(!name || !email || !password) {
    errors.push("Falta el nombre, el correo electrónico o la contraseña");
    return res.status(400).json({ errors });
  }
  const exist = await prisma. user. findUnique({
    where: {
      email: email
    }
  });

  if(exist) {
    errors.push("El usuario ya existe");
    return res.status(400).json({ errors });
  }
  
  if (password.length < 6) {
    errors.push("La longitud de la contraseña debe ser superior a 6 caracteres");
    return res.status(400).json({ errors });
  }
  try {
    const hashed_password = await hash(password, 12);
    
    const user = await prisma.user.create({
      data: { 
        ...req.body, 
        password: hashed_password 
      },
    });
    return res.status(201).json({ user });

  } catch (e) {
    if (e instanceof Prisma.PrismaClientKnownRequestError) {
      if (e.code === "P2002") {
        return res.status(400).json({ message: e.message });
      }
      return res.status(400).json({ message: e.message });
    }
  }
}
