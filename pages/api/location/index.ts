import type { NextApiRequest, NextApiResponse } from 'next';
import prisma from '../../../lib/prisma';
import { authOptions } from '../auth/[...nextauth]';
import { getServerSession } from "next-auth/next";

// POST /api/location
// Required fields in body: name, companyId, address
export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  const { name, companyId, address } = req.body;

  const session = await getServerSession(req, res, authOptions);
  
  if (!session) {
    res.status(401).json({ message: "Debes haber iniciado sesión." });
    return;
  }

  let resultAddress;

  try {
    resultAddress = await prisma.address.create({
      data: address
    });
  } catch (error) {
    res.status(500).json({ message: "Error al crear la dirección." });
    return;
  }

  const result = await prisma.location.create({
    data: {
      name,
      companyId,
      addressId: resultAddress.id,
    },
  });
  
  return res.json(result);
}
