import type { NextApiRequest, NextApiResponse } from 'next';
import prisma from '../../../lib/prisma';
import { authOptions } from '../auth/[...nextauth]';
import { getServerSession } from "next-auth/next";

// POST /api/category
// Required fields in body: name, description, companyId
export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  const { name, description, companyId } = req.body;

  const session = await getServerSession(req, res, authOptions);
  
  if (!session) {
    res.status(401).json({ message: "Debes haber iniciado sesión." });
    return;
  }

  const result = await prisma.category.create({
    data: {
      name,
      description,
      companyId
    }
  });
  
  return res.json(result);
}
