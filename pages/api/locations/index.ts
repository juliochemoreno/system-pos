import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/prisma'
import { authOptions } from '../auth/[...nextauth]'
import { getServerSession } from "next-auth/next"

// GET /api/locations
export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  const session = await getServerSession(req, res, authOptions)
  const { companyId } = req.body;

  if (!session) {
    res.status(401).json({ message: "Debes haber iniciado sesión." });
    return;
  }

  if (!companyId) {
    res.status(401).json({ message: "Se requiere el id de la empresa." });
    return;
  }
 
  const result = await prisma.location.findMany({
    where: {
      companyId
    },
    include: {
      address: true,
    }
  });

  return res.json(result);
}
