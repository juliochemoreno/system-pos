import type { NextApiRequest, NextApiResponse } from 'next';
import prisma from '../../../lib/prisma';
import { authOptions } from '../auth/[...nextauth]';
import { getServerSession } from "next-auth/next";
import { ProductType } from '@prisma/client';

// POST /api/product
// Required fields in body: name, companyId, category, price
export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  const { name, category, price, companyId } = req.body;

  const session = await getServerSession(req, res, authOptions);
  
  if (!session) {
    res.status(401).json({ message: "Debes haber iniciado sesión." });
    return;
  }

  const product = await prisma.product.create({
    data: {
      name: name,
      description: "",
      reference: "",
      type: ProductType.service,
      company: {
        connect: {
          id: companyId,
        },
      },
      category: {
        connect: {
          id: category.id
        }
      }
    }
  }).then(async (_product) => {

    await prisma.priceList.findMany({
      where: {
        companyId,
        name: "General"
      }
    }) .then( async (priceList) => {
      await prisma.price.create({
        data: {
          price: parseFloat(price),
          type: {
            connect: {
              id: priceList[0].id
            },
          },
          product: {
            connect: {
              id: _product.id
            }
          }
        },
      });
    });
  
  });
  
  return res.json(product);

}
