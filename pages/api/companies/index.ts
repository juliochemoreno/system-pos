import type { NextApiRequest, NextApiResponse } from 'next'
import prisma from '../../../lib/prisma'
import { authOptions } from '../auth/[...nextauth]'
import { getServerSession } from "next-auth/next"

// GET /api/companies
export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  const session = await getServerSession(req, res, authOptions)
  
  if (!session) {
    res.status(401).json({ message: "Debes haber iniciado sesión." });
    return;
  }

  const userId = session.user?.id;  

  const userCompanies = await prisma.company.findMany({
    where: {
      users: {
        some: {
          id: session.user?.id,
        },
      },
    },
    select: {
      id: true,
      name: true,      
      description: true,
    },
  });
  
  const companies = await Promise.all(
    userCompanies.map(async (company) => {
      const userRole = await prisma.userRole.findFirst({
        where: {
          userId: session.user?.id,
          companyId: company.id,
        },
        select: {
          role: true,
          companyId: true,
        },
      });
  
      return {
        ...company,
        role: userRole.role      
      };
    })
  );

  return res.json(companies);
}
