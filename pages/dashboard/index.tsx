import React, { useEffect, useState } from "react";
import type { GetServerSideProps, InferGetServerSidePropsType } from "next";
import Layout from "../../components/Layouts/Layout";
import { getSession, useSession } from 'next-auth/react';
import { Company } from "@prisma/client";
import Link from "next/link";
import { HiEye } from "react-icons/hi";
import prisma from "../../lib/prisma";

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);

  if (!session || !session.user) {
    return {
      props: {
        isLogin: false
      },
    };
  }
  
  const companies = await prisma.company.findMany({
    where: {
      users: {
        some: {
          id: session.user?.id,
        },
      },
    },
    include: {
      users: {
        select: {
          id: true,
          name: true,
        },
      },
    },
    take: 3,
  });  

  return {
    props: {
      isLogin: true,
      user: session.user,
      companies: companies
    },
  };
};

type Props = {
  isLogin: Boolean;
  user: {
    name?: string;
    email?: string;
    image?: string;
  };
  companies: Company[]
};

const Home: React.FC<Props> = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { data: session, status } = useSession();

  useEffect(() => {
    console.log(props);
  }, []);

  
  return (
    <Layout>
      <div className="navbar bg-base-300 text-base-300-content prose">
        <div className="navbar-start"></div>
        <div className="navbar-center">
          <h2 className="m-0">Dashboard</h2>
        </div>
        <div className="navbar-end"></div>  
      </div>

      <div className="grid grid-cols-2 gap-4 p-4">
        <div className="card card-compact w-96 bg-base-100 shadow-sm">
          <div className="card-body">
            <h2 className="card-title justify-between">
              Empresas
              <Link href="/companies" className="btn btn-sm btn-ghost">Ver todas</Link>
            </h2>            
            <table className="table">
               <thead>
                <tr className="bg-base-300 text-base-300-content">
                  <td>ID</td>
                  <td>Nombre</td>
                  <td className="w-48 text-center">Detalles</td>
                </tr>
              </thead>

              <tbody>
                {props.companies.map((company: Company) => (
                  <tr key={company.id} className="hover">
                    <th>{company.id}</th>
                    <td>
                      <div className="flex items-center space-x-3">
                        <div>
                          <div className="font-bold">{company.name}</div>
                          {/* <div className="text-sm opacity-50">United States</div> */}
                        </div>
                      </div>
                    </td>
                    <td className="flex justify-center">
                      <Link className="btn btn-circle btn-ghost" href={`/companies/${company.id}`}><HiEye/></Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Home;
