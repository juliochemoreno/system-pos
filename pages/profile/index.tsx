import React from "react";
import type { GetServerSideProps, InferGetServerSidePropsType } from "next";
import Layout from "../../components/Layouts/Layout";
import { getSession, useSession } from 'next-auth/react';

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);

  if (!session || !session.user) {
    return {
      props: {
        isLogin: false
      },
    };
  }

  return {
    props: {
      isLogin: true,
      user: session.user
    },
  };
};

type Props = {
  isLogin: Boolean;
  user: {
    name?: string;
    email?: string;
    image?: string;
  };
};

const Profile: React.FC<Props> = (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { data: session, status } = useSession();

  return (
    <Layout>
      <div className="navbar bg-base-300 text-base-300-content prose">
        <div className="navbar-start"></div>
        <div className="navbar-center">
          <h2 className="m-0">Mi Perfil</h2>
        </div>
        <div className="navbar-end"></div>  
      </div>

      <section className="p-4">
        <div className="prose p-4">
          { session && <div className="mockup-code"><pre><code>{JSON.stringify(session.user, null, 4)}</code></pre></div> }
        </div>
      </section>

    </Layout>
  );
};

export default Profile;
