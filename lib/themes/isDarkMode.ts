export default function isDarkMode() {
  const localStorageAvailable = typeof window !== 'undefined' && window.localStorage;
  const storedTheme = localStorageAvailable ? localStorage.getItem("theme") : null;
  return storedTheme == "dark";
}

import { useEffect, useState } from 'react';

type ChangeEvent = React.ChangeEvent<HTMLInputElement>

type useThemeReturn = [ string, (e: string) => void ];

export const useTheme = (): useThemeReturn => {

  const [theme, setTheme] = useState<string>()

  const handleChange = (value: string) => setTheme(value)

  useEffect(() => {
    if(theme && theme != '') {
      document.body.setAttribute('data-theme', theme);
      localStorage.setItem("theme", theme);
      window.dispatchEvent(new Event("changeTheme"));
    }
  }, [theme])

  return [theme, handleChange]
}