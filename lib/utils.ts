import { Address } from "@prisma/client";

export function formatAddress(address: Address): string {
  const parts = [];

  if (address.street1) {
    parts.push(address.street1);
  }

  if (address.street2) {
    parts.push(address.street2);
  }

  if (address.city) {
    parts.push(address.city);
  }

  if (address.state) {
    parts.push(address.state);
  }

  if (address.zipCode) {
    parts.push(address.zipCode);
  }

  if (address.country) {
    parts.push(address.country);
  }

  return parts.join(", ");
}
