import { DefaultSession } from "next-auth";

declare module "next-auth" {
  interface Session {
    user?: {
      id: string;
    } & DefaultSession["user"];
  }
}

export type Company = {
  id: number;
  name: string;
  description: string;
  role: string;
}

export type Product = {
  id: number;
  name: string;
  description: string;
  companyId: number;
  orderId: number;
  categoryId: number;
  hasNoIvaDays: boolean;
  reference: string;
  status: boolean;
  type: string;
  price: [Price];
  category: Category;
}