import React from "react";
import Router from "next/router";
import { Company } from "@prisma/client";

const Company: React.FC<{ company: Company }> = ({ company }) => {
  return (
    <div className="company" onClick={() => Router.push("/company/[id]", `/company/${company.id}`)}>
      <h2>{company.name}</h2>
    </div>
  );
};

export default Company;
