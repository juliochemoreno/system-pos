// TabContent.tsx
import React, { useState, ReactNode } from "react";

type TabContentProps = {
  tabs: string[];
  defaultTab: number;
  children: ReactNode[];
};

const TabContent: React.FC<TabContentProps> = ({ tabs, defaultTab, children }) => {
  const [activeTab, setActiveTab] = useState(defaultTab);

  return (
    <div>
      <div className="tabs">
        {tabs.map((tab, index) => (      
          <span key={index} className={`tab tab-lg tab-lifted ${activeTab === index ? "tab-active" : ""}`} onClick={() => setActiveTab(index)} >
            {tab}
          </span>
        ))}
      </div>

      {tabs.map((tab, index) => (
        <div key={index} className={activeTab === index ? "flex flex-col gap-4 w-full p-5 bg-base-100" : "hidden"} >
          {children[index]}
        </div>
      ))}
    </div>
  );
};

export default TabContent;
