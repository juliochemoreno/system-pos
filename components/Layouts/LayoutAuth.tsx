import React, { Fragment, ReactNode } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import logoWhite from "../../images/logos/tayrosoft-logo-white.svg";
import logoColor from "../../images/logos/tayrosoft-logo.svg";
import Image from "next/image";

type Props = {
  children: ReactNode;
};

const featuresLogin = [
  {
    id: 1,
    title: "Manejo y control de productos",
    subtitle: "Agrega productos o servicios, asocie precios y códigos de referencia",
  },
  {
    id: 2,
    title: "Control de cajas",
    subtitle: "Realiza apertura y cierre de caja diarios para llevar el control de tu dinero",
  },
  {
    id: 3,
    title: "Informes y analítica",
    subtitle: "Recibe informes y alertas a tu correo con la información precisa para que tu negocio crezca"
  },
  {
    id: 4,
    title: "Gestión remota de tu empresa",
    subtitle: "Administra tu negocio desde cualquier lugar y en cualquier momento",
  }
];

const Layout: React.FC<Props> = (props) => {
  return (
    <section className="flex flex-row items-center h-screen p-5 md:p-20 lg:p-10 xl:p-20">
      <div className="hidden basis-0 lg:block lg:basis-1/2 xl:basis-2/3">
        <div className="w-full">
          <div className="mockup-browser border bg-base-300">
            <div className="mockup-browser-toolbar">
              <div className="input">https://tayrosoft.com</div>
            </div>
            <div className="flex flex-col gap-8 justify-center px-8 py-10 border-t border-base-300">
              {featuresLogin.map((feature) => (
                <Fragment key={feature.id}>
                  <div className="flex flex-row">
                    <div>
                      <AiFillCheckCircle className="h-7 w-7 mr-3 mt-1" />
                    </div>

                    <article className="prose">
                      <h2 className="mb-0">{feature.title}</h2>
                      <p>{feature.subtitle}</p>
                    </article>
                  </div>
                </Fragment>
              ))}
            </div>
          </div>
        </div>
      </div>

      <div className="basis-full lg:basis-1/2 xl:basis-1/3">
        {props.children}
      </div>

    </section>
  );
};

export default Layout;
