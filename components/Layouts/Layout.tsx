import React, { Fragment, ReactNode, useState } from "react";
import Header from "../Header";

type Props = {
  children: ReactNode;
};

const Layout: React.FC<Props> = (props) => {
  const [openSidebar, setOpenSidebar] = useState(false);

  const handleSetOpenSidebar = (newValue: boolean) => {
    setOpenSidebar(prev => !prev);
  };
    
  return (
    <Fragment>
      <Header/>
      <main className="min-h-screen min-w-screen bg-base-200 pt-16 flex flex-col">
        {props.children}
      </main>
    </Fragment>
  );
};

export default Layout;
