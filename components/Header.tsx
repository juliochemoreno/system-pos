'use client';

import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { signOut, useSession } from "next-auth/react";
import Image from "next/image";
import { FaCircleUser, FaGear } from "react-icons/fa6";
import { FaSignOutAlt } from "react-icons/fa";
import {HiMenuAlt2 } from "react-icons/hi";
import logoColor from "/images/logos/tayrosoft-logo.svg";
import logoWhite from "/images/logos/tayrosoft-logo-white.svg";
import SwitchTheme from "./SwitchTheme";
import { useTheme } from "../lib/themes/isDarkMode";
import Router from "next/router";

const Header: React.FC = () => {

  const router = useRouter();
  const { data: session, status } = useSession();
  const [theme, setTheme] = useState("");

  function changeThemeEventHandler() {
    const newTheme = localStorage.getItem('theme') || null;
    if(newTheme) {
      setTheme(newTheme);
    }
  }

  const goToHome = async () => {
    await Router.push("/")
  };

  useEffect(() => {
    window.addEventListener('changeTheme', changeThemeEventHandler, false);
  }, []);

  return (
    <div className="navbar bg-base-100 text-base-100-content fixed border-b z-50">
      <div className="navbar-start">
        <div className="drawer">
          <input id="my-drawer" type="checkbox" className="drawer-toggle" />
          <div className="drawer-content">
            <label htmlFor="my-drawer" className="btn btn-ghost btn-circle">
              <HiMenuAlt2 className="h-6 w-6"/>              
            </label>
          </div>
          
          <div className="drawer-side">
            <label htmlFor="my-drawer" aria-label="close sidebar" className="drawer-overlay"></label>
            <ul className="menu menu-md bg-base-200 text-base-content w-80 min-h-full p-4 [&_li>*]:rounded-none">
              <li><Link href="/profile"><FaCircleUser className="h-4 w-4"/>Mi perfil</Link></li>
              <li><Link href="/settings"><FaGear className="h-4 w-4"/>Configuración</Link></li>
              <li onClick={() => signOut()}><a><FaSignOutAlt className="h-4 w-4"/> Cerrar sesión</a></li>
            </ul>
          </div>
        </div>
      </div>

      <div className="navbar-center">    
        <Image priority={true} src={theme != "dark" ? logoColor : logoWhite } alt="Tayrosoft Logo" className="w-40 cursor-pointer" onClick={goToHome}/>
      </div>

      <div className="navbar-end gap-4">
        <SwitchTheme/>   
        { session && <>
          <div className="dropdown dropdown-end dropdown-hover">
            <label tabIndex={0} className="flex gap-3 items-center">
              <h2>{session.user.email}</h2>                
              <div className="avatar placeholder">
                <div className="bg-primary text-primary-content rounded-full w-10">
                  <span>
                    {session.user.name.substring(0, 2)}
                  </span>
                </div>
              </div>
            </label>

            <ul tabIndex={0} className="menu menu-md dropdown-content bg-base-100 text-base-100-content mt-3 z-[1] p-2 shadow [&_li>*]:rounded-none w-52">
              <li><Link href="/profile"><FaCircleUser className="h-4 w-4"/>Mi perfil</Link></li>
              <li><Link href="/settings"><FaGear className="h-4 w-4"/>Configuración</Link></li>
              <li onClick={() => signOut()}><a><FaSignOutAlt className="h-4 w-4"/> Cerrar sesión</a></li>
            </ul>
          </div>
        </> }

        {!session && <Link href="/api/auth/signin" legacyBehavior>
          <a className="btn btn-base-100">Iniciar sesión</a>          
        </Link>}
      </div>
    </div>
  );
};

export default Header;
